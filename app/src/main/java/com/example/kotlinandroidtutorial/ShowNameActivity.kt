package com.example.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ShowNameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_name)
        val showNameTxt = findViewById<TextView>(R.id.showNameTxt)
        val name:String = intent.getStringExtra("name")?:"Unknows"
        showNameTxt.setText(name)
    }
}