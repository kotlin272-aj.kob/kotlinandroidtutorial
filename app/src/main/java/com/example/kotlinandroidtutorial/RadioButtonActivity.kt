    package com.example.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_radio_button.*

    class RadioButtonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_radio_button)
        radio_group.setOnCheckedChangeListener{group,checkedId ->
            val radio = findViewById<RadioButton>(checkedId)
            Toast.makeText(applicationContext,"On Checked Change ${radio.text}",Toast.LENGTH_SHORT).show()
        }
        button.setOnClickListener{
            val checkedId = radio_group.checkedRadioButtonId
            if(checkedId !== -1){
                val radio = findViewById<RadioButton>(checkedId)
                Toast.makeText(applicationContext,"On Button Click ${radio.text}",Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(applicationContext,"On Button Click nothing selected",Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun radio_button_click(view:View){
        val radio = findViewById<RadioButton>(radio_group.checkedRadioButtonId)
        Toast.makeText(applicationContext,"On Click ${radio.text}",Toast.LENGTH_SHORT).show()
    }
}